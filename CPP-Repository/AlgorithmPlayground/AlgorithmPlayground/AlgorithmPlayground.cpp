// AlgorithmPlayground.cpp : This file contains the 'main' function. Program
// execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include "math.h"

struct item{
  int weight, value;
  item(int w = 0, int v = 0){ weight = w; value = v;}
};

double median(int arr1[], int arr2[]);
int   BottomUpKnapsack(item items[], int numItems, int maxWeight, int** cache);
int    max(int** Arr, int i, int j, item items[], int numItems, int maxWeight);

int main() {
  
//  int arr1[] = {1,5,9}, arr2[] = {2,6,12};
//  double med = median(arr1, arr2);
//  std::cout << med;
  std::srand(time(0));
  item items[] = {{1,std::rand()%5}, {2,5 + std::rand() % 10}, {3,10 + std::rand()%20}};
  int** cache = new int*[4];
  for(int i = 0; i < 4; i++){
    cache[i] = new int[6]{0};
  }

  /*for(int i = 0; i <= 3; i++){
    std::cout << "|i == " << i << "|";
    for(int j = 0; j <= 5; j++){
      std::cout << "[" << std::setw(2) << cache[i][j] << "]";
    }
    std::cout << std::endl;
  }*/

  int maxValue = BottomUpKnapsack(items, 3, 5, cache);
  
  for(int i = 0; i <= 3; i++){
    for(int j = 0; j <= 5; j++){
      std::cout << "[" << std::setw(2) << cache[i][j] << "]";
    }
    std::cout << std::endl;
  }
  std::cout << "Max value is " << maxValue;
  
  
  return 0;
}

double median(int arr1[], int arr2[]) {
  if (arr1 != nullptr && arr2 != nullptr) {
    return ((double)(arr1[1] + arr2[1]) / 2.0);
  } else {
    return NAN;
  }
}

int BottomUpKnapsack(item items[], int numItems, int maxWeight, int** cache){
  for(int i = 1; i <= 3; i++){
    for(int j = 0; j <= maxWeight; j++){
      if(items[i - 1].weight < j){
        cache[i][j] = max(cache, i, j, items, 3, maxWeight);
      }else{
        cache[i][j] = cache[i - 1][j];
      }
    }
  }
  return cache[numItems][maxWeight];
}

int max(int** Arr, int i, int j, item items[], int numItems, int maxWeight){
  if( (i >= 0) && (j >= 0) && (j <= maxWeight) && (i-1 <= numItems) ){
    int lhs = 0; 
    int rhs = 0;
    
    if((j-items[i-1].weight) >= 0){
      rhs = Arr[i - 1][(j - items[i-1].weight)] + items[i - 1].value;
    }else{
      return lhs;
    }
    return (lhs >= rhs ? lhs : rhs);
  }else{
    return Arr[i - 1][j];
  }
}