/**
* A 1-0 Knapsack optimization problem and a bottom-up algorithm to solve it
*   using dynamic programming
*
* @author   John Loeffler
* @linkedIn LinkedIn.com/in/JohnLoeffler
* @github   Github.com/JohnLoeffler
* @email    John.Loeffler@gmail.com
* @twitter  Twitter.com/ThisDotJohn
*/

#include "pch.h"
#include <iostream>
#include <iomanip>
#include <ctime>

/**
* Struct for holding item weights and values
*/
struct item{
  int weight, value;
  item(int w = 0, int v = 0){ weight = w; value = v;}
};

//  Function declarations   //
int    BottomUpKnapsack(item items[], const int& numItems, const int& maxWeight, int** table);
int    Max(int** Arr, const int& i, const int& j, item items[], const int& numItems, const int& maxWeight);

int main() {

  //  Somewhat randomize the item weights and values
  std::srand(time(0));
  for(int i = 0; i < 100; i++){
    rand();
  }
  int numItems = 3 + rand() % 5;
  item *items = new item[numItems];
  int mean = 0;
  for(int i = 0; i < numItems; i++){
    items[i] = {i, 1+(i * std::rand() % 5)};
    mean += i;
  }

  //  Set the maxWeight relative to the item weights
  int  maxWeight = ((mean*=mean) /= numItems);
  if(maxWeight > 20){
    maxWeight = 20;
  }
  //  Create the table used to find the optimal value
  int** table    = new int*[numItems+1];
  for(int i = 0; i <= numItems; i++){
    table[i] = new int[maxWeight+1]{0};
  }

  //  Get the max value
  int maxValue = BottomUpKnapsack(items, numItems, maxWeight, table);
  
  //  Print the table and returned value for verification purposes
  for(int i = 0; i <= numItems; i++){
    for(int j = 0; j <= maxWeight; j++){
      std::cout << "[" << std::setw(2) << table[i][j] << "]";
    }
    std::cout << std::endl;
  }
  std::cout << "---------------------------\n";
  std::cout << "The Maximum value possible is " << maxValue;
  
  return 0;
}

/**
* BottomUpKnapsack: Finds the maximum value possible given an array of item structs by using dynamic programming
* @param  items     The array of items needing optimization
* @param  numItems  The number of items in the array
* @param  maxWeight The maximum weight capacity of the 'bag' holding the items
* @param  table     A two dimensional array for the function to generate the optimized value
*/
int BottomUpKnapsack(item items[], const int& numItems, const int& maxWeight, int** table){
  
  for(int i = 1; i <= numItems; i++){
    for(int j = 0; j <= maxWeight; j++){

      // If the current item can potentially fit in the bag
      if(items[i - 1].weight < j){
        table[i][j] = Max(table, i, j, items, numItems, maxWeight);

      //  Carry down the largest value found so far
      } else{
        table[i][j] = table[i - 1][j];
      }
    }
  }
  // Once the table has been generated, the optimal value will be in the cell at the bottom-right corner of the table
  return table[numItems][maxWeight];
}

/**
* Max: Determines which value of the available options on the table is the largest value possible for the given position
* @param  table     The 2D array of values being read from and written to
* @param  i         The row number of the table
* @param  j         The column number of the table
* @param  items     The array of item structs containing the weights and values
* @param  numItems  The size of the items array
* @param  maxWeight The maximum weight the 'knapsack' can hold
*/
int Max(int** table, const int& i, const int& j, item items[], const int& numItems, const int& maxWeight){
  //  Validates input parameters
  if( (i >= 0) && (j >= 0) && (j <= maxWeight) && (i-1 <= numItems) ){
    
    //  Carry down the highest value so far
    int lhs = table[i-1][j]; 
    
    // Replace the most recent item added with the new item
    int rhs = table[i - 1][(j - items[i - 1].weight)] + items[i - 1].value;
    
    // Return the most valuable combination of items for the given weight
    return (lhs >= rhs ? lhs : rhs);
  }else{
    return (int) NAN;
  }
}