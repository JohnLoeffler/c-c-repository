// Duplicate-Array-Values.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>

/* Function Prototypes */
bool SortArray(int* arr, int arrSize = 0);
int  FindDuplicateValuesInArray(int *arr, int arrSize = 0);
int  GetDuplicatesAsSubsetArray(int *dupes, int *arr, int dupesSize = 0, int arrSize = 0);
void PrintArray(int *arr, int arrSize = 0);

int main(){
  
  int noDupe[5]{1,2,3,4,5}, allDupes[5]{3,3,3,3,3}, twoDupes[5]{1,2,3,3,2}, lotsOfDupes[20]{1,0,2,3,2,3,6,3,5,6,8,7,6,7,8,9,0,1,2,3};
  int dupesSize = FindDuplicateValuesInArray(noDupe, 5);
  if(dupesSize){
    std::cout << "Error! Dupes found in the noDupe array!" << std::endl;
  }else{
    std::cout << "no dupes found in the noDupe array. :)" << std::endl;
  }

  int setSize = 0;
  int *dupes = nullptr;

  dupesSize = FindDuplicateValuesInArray(allDupes, 5);
  dupes = new int[dupesSize]{0};
  setSize = GetDuplicatesAsSubsetArray(dupes, allDupes, dupesSize, 5);
  PrintArray(dupes, dupesSize);
  delete dupes;

  dupesSize = FindDuplicateValuesInArray(twoDupes, 5);
  dupes = new int[dupesSize]{0};
  setSize = GetDuplicatesAsSubsetArray(dupes, twoDupes, dupesSize, 5);
  PrintArray(dupes, dupesSize);
  delete dupes;
  
  dupesSize = FindDuplicateValuesInArray(lotsOfDupes, 20);
  dupes = new int[dupesSize]{0};
  setSize = GetDuplicatesAsSubsetArray(dupes, lotsOfDupes, dupesSize, 20);
  PrintArray(dupes, dupesSize);
  delete dupes;

  return 0;
}

/**
* SortArray: sorts the parameter array in ascending order 
* @param  arr     The array of ints to sort
* @param  arrSize The size of the array
* @return returns true if the array was successfully sorted, false sorting the array results in an error
*/
bool SortArray(int* arr, int arrSize){
  try{
    std::qsort(arr, arrSize, sizeof(arr[0]), [](const void* a, const void* b){
      int arg1 = *static_cast<const int*>(a);
      int arg2 = *static_cast<const int*>(b);

      if(arg1 < arg2) return -1;
      if(arg1 > arg2) return 1;
      return 0;
    });
    return true;
  }catch(std::exception ex){
    return false;
  }
}

/**
* FindDuplicateValuesInArray: Finds the number of values in an array that appear at least twice in the array
* @param  arr     The array of ints to search through for duplicates
* @param  arrSize The size of the array
* @return returns the number of values that appear at least twice in the array; returns -1 if the array is a nullptr or the array size is zero; returns -2 if an error occurred while sorting the array 
*/
int  FindDuplicateValuesInArray(int *arr, int arrSize){
  if(arrSize && arr != nullptr){
    if(SortArray(arr, arrSize)){
      int value = arr[0], numDupes = 0;
      bool newValue = true;
      for(int i = 0; i < arrSize - 1; i++){
        if(arr[i] > value){
          value = arr[i];
          newValue = true;
        }
        if(arr[i + 1] == value && newValue){
          numDupes++;
          newValue = false;
          while(arr[i + 1] == value && i != arrSize - 2){
            i++;
          }
        }
      }
      if(arr[arrSize-2] == value && arr[arrSize-1]== value){
        if(arr[arrSize-3] != value){
          return (++numDupes);
        }
      }
      return numDupes;
    } else{
      return -2;
    }
  }else{
    return -1;
  } 
}

/**
* GetDuplicatesAsSubsetArray: Produces a subset array containing each value in the array that appears at least twice
* @param  dupes     An allocated array sized to hold one of each duplicated value in the original array
* @param  arr       The array of ints containing the duplicated values 
* @param  dupesSize The size of the dupes awway
* @param  arrSize   The size of the arr array
* @return returns the number of values collected in the dupes array; returns zero if dupes is an empty set; returns -1 if either array is a nullptr or the size of one or both arrays is zero
*/
int GetDuplicatesAsSubsetArray(int *dupes, int *arr, int dupesSize, int arrSize){
  if(arrSize > 2 && dupesSize && arr != nullptr && dupes != nullptr){
    int dupeIndex = -1;
    bool newValue = true;
    for(int i = 0; i < arrSize - 2; i++){
      if(arr[i+1] > arr[i]){
        i++;
        newValue = true;
      }
      if(arr[i] == arr[i + 1] && newValue){
        dupeIndex++;
        if(dupeIndex <= dupesSize - 1){
          dupes[dupeIndex] = arr[i];
        }else{
          throw new std::exception("dupes array doesn't have enough memory allocated to form the full subset");
        }
        

        newValue = false;
        while(arr[i] == arr[i + 1] && i < arrSize - 2){
          i++;
        }
      }
    }
    return dupeIndex + 1;
  } else{
    return -1;
  }
}

/**
* PrintArray: Prints the contents of the parameter array
* @param  arr     The array to print
* @param  arrSize The size of the array to print
*/
void PrintArray(int *arr, int arrSize){
  if(arrSize && arr != nullptr){
    for(int i = 0; i < arrSize; i++){
      std::cout << "[" << std::setw(2) << arr[i] << "]";
    }
    std::cout << std::endl;

  }else{
    std::cerr << "Unable to print array. Either the array is a nullptr or the array size is zero." << std::endl;
  }
}