// Matrix-Path-To-Greatest-Product.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <iomanip>

/* Function Declarations */
void Recurse(int index, int* matrix, int right, int down, int product, int& highProduct, int pathIndex, int* path, int* highPath);
void PrintMatrix(const int* matrix);
void PrintPath(const int* highPath);
void PrintPathAsExpression(const int* highPath, const int* matrix);

int main()
{
  //  Set up function parameter variables
  int down = 2, right = 2, product = 1, highProduct= 0, index = 0, pathIndex = 0;
  
  //  Setup matrix to compute
  int *matrix = new int[9]{-1,-2,3,4,-5,-6,7,8,9};
  
  //  Setup the working path array and an array to hold the path to current greatest product found
  int *path     = new int[5]{0};
  int *highPath = new int[5]{0};

  //  Call recursive function
  Recurse(index, matrix, right, down, product, highProduct, pathIndex, path, highPath);

  /*  Print the results of the algorithm  */
  
    //  Print the matrix for reference
  PrintMatrix(matrix);

    //  Print the visual path taken to produce the largest product
  PrintPath(highPath);

    //  Print the path as a mathematical expression
  PrintPathAsExpression(highPath, matrix);

  return 0;
}

/**
* Recurse:  Recursive pathfinding function that finds path to produce largest product of matrix values after traversal
* @param    index       The current index in the matrix array
* @param    matrix      An array of ints containing the matrix values  
* @param    right       How many valid moves to the right are possible in the current position
* @param    down        How many valid moves downward are possible in the current position
* @param    product     The accumulated product of values along the path taken so far
* @param    highProduct The largest product computed by the algorithm so far
* @param    pathIndex   The current index of the path array
* @param    path        An array of ints that records each matrix array index along the current path
* @param    highPath    An array of ints that caches the path that produces the largest product found so far
*/
void Recurse(int index, int* matrix, int right, int down, int product, int& highProduct, int pathIndex, int* path, int* highPath){
  //  check for and prevent invalid moves beyond the matrix bounds
  if(right < 0 || down < 0){ return; }

  //  factor in current index value into product
  product *= matrix[index];  

  //  record the current index in path
  path[pathIndex] = index;

  // BASE CASE: reached the bottom-right endpoint
  if((right+down) == 0){ 
    if(product > highProduct){  //  new highest product found
      highProduct = product;
      for(int i = 0; i < 5; i++){
        highPath[i] = path[i];  //  cache the indices making up the path producing the highProduct
      }
    }
    return;
  }

  //  Recurse through the matrix, first right, then down
  Recurse((index + 1), matrix, (right - 1), down, product, highProduct, (pathIndex + 1), path, highPath);
  Recurse((index + 3), matrix, right, (down - 1), product, highProduct, (pathIndex + 1), path, highPath);
}

/**
* PrintMatrix: Prints out the formatted number matrix
* @param  matrix  The matrix array to print
*/
void PrintMatrix(const int* matrix){
  std::cout << "====================" << std::endl;
  for(int i = 0; i < 9; i++){
    std::cout << "[" << std::setw(2) << matrix[i] << "]";
    if(i % 3 == 2){
      std::cout << std::endl;
    }
  }
  std::cout << "====================" << std::endl;
}

/**
* PrintPath: Prints out a visual representation of the algorithms path to the greatest product
* @param  highPath  The array containing the indices of the matrix array along the optimal path
*/
void PrintPath(const int* highPath){
  int pIndex = 0;
  std::cout << "====================" << std::endl;
  for(int i = 0; i < 9; i++){
    std::cout << "[";
    if(i == highPath[pIndex]){
      std::cout << " " << 1;
      pIndex++;
    }else{
      std::cout << "  ";
    }
    std::cout << "]";
    if(i % 3 == 2){
      std::cout << std::endl;
    }
  }
  std::cout << "====================" << std::endl;
}

/**
* PrintPathAsFactors: Prints out a visual representation of the algorithms path as a mathematical expression
* @param  highPath  The array containing the indices of the matrix array along the optimal path
* @param  matrix    The matrix array containing the values the path computed
*/
void PrintPathAsExpression(const int* highPath, const int* matrix){
  std::cout << "===========================" << std::endl;
  int accumulator = 1;
  for(int i = 0; i < 5; i++){
    accumulator *= matrix[highPath[i]];
    std::cout << matrix[highPath[i]];
    if(i < 4){
      std::cout << " x ";
    }else{
      std::cout << " == " << accumulator << std::endl;
    }
  }
  std::cout << "===========================" << std::endl;
}